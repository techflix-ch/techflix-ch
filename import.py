import os
import json
import pyyoutube

API_KEY = os.getenv('YOUTUBE_API_KEY')
PLAYLIST_ID = "PLs4UgZDnE2U5KwDDicLr__VvmL7ztJIai"

def main():
    api = pyyoutube.Api(api_key=API_KEY)
    resp = api.get_playlist_items(playlist_id=PLAYLIST_ID, count=1000)
    videos = []
    for index, item in enumerate(resp.items, start=1):
        try:
            title = item.snippet.title.replace('"', '')
            print(f"{index} - {title}")
            videos.append({
                'id': item.snippet.resourceId.videoId,
                'title': title,
                'thumbnail_url': item.snippet.thumbnails.maxres.url if item.snippet.thumbnails.maxres else item.snippet.thumbnails.medium.url,
                'published_at': item.snippet.publishedAt
            })
        except AttributeError as e:
            continue

    with open('public/videos.json', 'w+') as outfile:
        json.dump(videos, outfile)

if __name__ == "__main__":
    main()
